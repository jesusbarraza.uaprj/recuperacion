package Modelo;
//

public class Recibo {
    
    private int Servicio,Nrecibo, Consumido;
    private String fecha, nombre, domicilio;
    private float costo;


    
    public Recibo(int Servicio, int Nrecibo, int Consumido, String fecha, String nombre, String domicilio, float costo) {
        this.Servicio = Servicio;
        this.Nrecibo = Nrecibo;
        this.Consumido = Consumido;
        this.fecha = fecha;
        this.nombre = nombre;
        this.domicilio = domicilio;
        this.costo = costo;
    }

    public Recibo(Recibo ob) {
        this.Servicio = ob.Servicio;
        this.Nrecibo = ob.Nrecibo;
        this.Consumido = ob.Consumido;
        this.fecha = ob.fecha;
        this.nombre = ob.nombre;
        this.domicilio = ob.domicilio;
        this.costo = ob.costo;
    }

    public Recibo() {
        this.Servicio = 0;
        this.Nrecibo = 0;
        this.Consumido = 0;
        this.fecha = "";
        this.nombre = "";
        this.domicilio = "";
        this.costo = 0;
    }

    
    public int getServicio() {
        return Servicio;
    }

    public void setServicio(int Servicio) {
        this.Servicio = Servicio;
    }

    public int getNrecibo() {
        return Nrecibo;
    }

    public void setNrecibo(int Nrecibo) {
        this.Nrecibo = Nrecibo;
    }

    public int getConsumido() {
        return Consumido;
    }

    public void setConsumido(int Consumido) {
        this.Consumido = Consumido;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDomicilio() {
        return domicilio;
    }

    public void setDomicilio(String domicilio) {
        this.domicilio = domicilio;
    }

    public float getCosto() {
        return costo;
    }

    public void setCosto(float costo) {
        this.costo = costo;
    }
    
    public float calcularSubtotal(){
        return (this.costo*this.Consumido);
    }
    
    public float calcularImpuesto(){
        return (calcularSubtotal()*0.16f);
    }
    
    public float calcularTotal(){
        return (calcularSubtotal()+calcularImpuesto());
    }
    
    
    
}


