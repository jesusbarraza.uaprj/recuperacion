package Controlador;

import Modelo.Recibo;
import Vista.dlgVista;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JFrame;
import javax.swing.JOptionPane;


public class Controlador implements ActionListener{

    dlgVista vista;
    Recibo re;

    public Controlador(dlgVista vista, Recibo re) {
        this.vista = vista;
        this.re = re;
        
        vista.btnNuevo.addActionListener(this);
        vista.btnCancelar.addActionListener(this);
        vista.btnCerrar.addActionListener(this);
        vista.btnGuarda.addActionListener(this);
        vista.btnLimpiar.addActionListener(this);
        vista.btnMostrar.addActionListener(this);
        
    }
    
    public void iniciarVista(){
        vista.setTitle("::RECIBO::");
        vista.setSize(600, 450);
        vista.setVisible(true);
        
    }
    
    
    
    public static void main(String[] args) {
        dlgVista vista = new dlgVista(new JFrame(), true);
        Recibo re = new Recibo();
        Controlador con = new Controlador(vista, re );
        con.iniciarVista(); 
        
    }
    
    public void apagar(){
        vista.txtDomicilio.setEnabled(false);
        vista.txtFecha.setEnabled(false);
        vista.txtNombre.setEnabled(false);
        vista.txtNumRecibo.setEnabled(false);
        vista.txtCostoKilo.setEnabled(false);
        vista.txtKiloConsumido.setEnabled(false);
        vista.cboTipoServicio.setEnabled(false);
                
    } 

    public void limpiar(){
            vista.txtNombre.setText("");
            vista.txtDomicilio.setText("");
            vista.txtFecha.setText("");
            vista.txtCostoKilo.setText("");
            vista.txtKiloConsumido.setText("");
            vista.txtNumRecibo.setText("");
            vista.cboTipoServicio.setSelectedIndex(0);
            vista.txtImpuesto.setText("0");
            vista.txtSubtotal.setText("0");
            vista.txtTotalPago.setText("0");
            
    }
    
        @Override
    public void actionPerformed(ActionEvent e) {
        
        if(e.getSource()==vista.cboTipoServicio){
            if(vista.cboTipoServicio.getSelectedIndex()==1)
                vista.txtCostoKilo.setText("2");
            else if(vista.cboTipoServicio.getSelectedIndex()==2)
                vista.txtCostoKilo.setText("3");
            else if(vista.cboTipoServicio.getSelectedIndex()==3)
                vista.txtCostoKilo.setText("5");
        }
        
        
        if(e.getSource()==vista.btnNuevo){
            
            vista.txtNumRecibo.setEnabled(true);
            vista.txtFecha.setEnabled(true);
            vista.txtNombre.setEnabled(true);
            vista.txtDomicilio.setEnabled(true);
            vista.cboTipoServicio.setEnabled(true);
            vista.txtKiloConsumido.setEnabled(true);
            
            vista.btnGuarda.setEnabled(true);

            limpiar();
           

        }
        
        if(e.getSource()==vista.btnGuarda){
            
            try{
                re.setNrecibo(Integer.parseInt(vista.txtNumRecibo.getText()));
                re.setNombre(vista.txtNombre.getText());
                re.setFecha(vista.txtFecha.getText());
                re.setDomicilio(vista.txtDomicilio.getText());
                re.setConsumido(Integer.parseInt(vista.txtKiloConsumido.getText()));

            }
            catch(NumberFormatException ex){
                JOptionPane.showMessageDialog(vista, "Ocurrio un error: " + ex.getMessage());
                return;
            }
            catch(Exception ex2){
                
                JOptionPane.showMessageDialog(vista, "Ocurrio este error: " + ex2.getMessage());
                return;
                
            }
            
            re.setCosto(Float.parseFloat(vista.txtCostoKilo.getText()));
   
            vista.btnMostrar.setEnabled(true);
            
        }
        
         if(e.getSource()==vista.btnMostrar){
            
            vista.txtDomicilio.setText(String.valueOf(re.getNrecibo()));
            vista.txtFecha.setText(re.getFecha());
            vista.txtNombre.setText(re.getNombre());
            vista.txtDomicilio.setText(re.getDomicilio());
            vista.txtCostoKilo.setText(String.valueOf(re.getCosto()));
            vista.txtKiloConsumido.setText(String.valueOf(re.getConsumido()));
            
            if (re.getServicio()==1){
                vista.cboTipoServicio.setSelectedIndex(1);
            }
            else if (re.getServicio()==2){
                vista.cboTipoServicio.setSelectedIndex(2);
            }
            else if (re.getServicio()==3){
                vista.cboTipoServicio.setSelectedIndex(3);
            }
            
            vista.txtSubtotal.setText(String.valueOf(re.calcularSubtotal()));
            vista.txtImpuesto.setText(String.valueOf(re.calcularImpuesto()));
            vista.txtTotalPago.setText(String.valueOf(re.calcularTotal()));
            
            vista.btnLimpiar.setEnabled(true);
            vista.btnCancelar.setEnabled(true);
        }
        
        if (e.getSource()==vista.btnLimpiar){
            limpiar();
            
        }
        
        if (e.getSource()==vista.btnCancelar){
            re = new Recibo();
            limpiar();
            apagar();
            
        }
        
        if(e.getSource()==vista.btnCerrar){
                int option = JOptionPane.showConfirmDialog(vista, "¿Desea salir?",
                        "Seleccione", JOptionPane.YES_NO_OPTION);
                if(option == JOptionPane.YES_NO_OPTION){
                    vista.dispose();
                    System.exit(0);
            }
        }
        
        
    }
    
}

